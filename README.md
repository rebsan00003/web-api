# Web-API
This is an ASP.NET Core Web API to store and manipulate movies with related characters and franchises. The three entitites have full CRUD operations plus additional reports to the basic reads. The Web API uses Swagger for documentation and design. 

## Install
Clone the repository to a local directory.

```
git clone https://gitlab.com/rebsan00003/web-api.git
cd web-api

```

Install NuGet packages:

- Microsoft.EntityFrameworkCore 
- Microsoft.EntityFrameworkCore.Tools
- Microsoft.EntityFrameworkCore.SqlServer


## Prerequisites

- .NET Framework
- Sql Server
- Visual Studio 2017/19 OR Visual Studio Code


## Contributors
Negin Bakhtiarirad (@neginb) & Rebecka Ocampo Sandgren (@rebsan00003)

## Contributing
No contribution allowed 
