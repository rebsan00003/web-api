﻿using AutoMapper;
using EF_Core_Database.DTOs.CharacterDTO;
using EF_Core_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            //Character<->CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>().ReverseMap();

            //Character<->CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

            //Character<->CharacterUpdateDTO
            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();
        }
    }
}
