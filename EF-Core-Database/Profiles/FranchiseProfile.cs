﻿using AutoMapper;
using EF_Core_Database.DTOs.FranchiseDTO;
using EF_Core_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Franchise<->FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f=>f.Movies.Select(m=>m.MovieId).ToArray()))
                .ReverseMap();

            //Franchise<->FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();

            //Franchise<->FranchiseUpdateDTO
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
        }
    }
}
