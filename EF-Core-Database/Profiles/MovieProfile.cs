﻿using AutoMapper;
using EF_Core_Database.DTOs.MovieDTO;
using EF_Core_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            //Movie <->MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                // Turning related characters into int Arrays
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.CharacterId).ToArray()))
                .ReverseMap();

            //MovieCreateDTO<->Movie
            CreateMap<MovieCreateDTO, Movie>();

            //Movie <->MovieUpdateDTO
            CreateMap<Movie, MovieUpdateDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();
        }
    }
}
