﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EF_Core_Database.Data;
using EF_Core_Database.Models;
using AutoMapper;
using EF_Core_Database.DTOs.MovieDTO;
using System.Net.Mime;
using EF_Core_Database.Services;

namespace EF_Core_Database.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieDbContext context, IMapper mapper, IMovieService movieService)
        {
            _movieService = movieService;
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());

        }

        /// <summary>
        /// Get a specific movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);


            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Reporting method to get all characters in a specific movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInMovie(int id)
        {

            var characters = await _context.Movie.Where(m => m.MovieId == id).SelectMany(m => m.Characters).ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            return characters;
        }

        /// <summary>
        /// Update a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieUpdateDTO movieDto)
        {
            if (id != movieDto.MovieId)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Update character list in a specific movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="charactersId"></param>
        /// <returns></returns>
        [HttpPut("{id}/Characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id,[FromBody] int[] charactersId )
        {
            if (!MovieExists(id))
            {
                return NotFound();
            }

            Movie movieToUpdateCharacters = await _context.Movie
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();

            List<Character> characterList = new();

            for(int i = 0; i< charactersId.Length; i++)
            {
                Character character = await _context.Character.FindAsync(charactersId[i]);
                if(character == null)
                {
                    return BadRequest("This character does not exists");
                }
                characterList.Add(character);
            }
            movieToUpdateCharacters.Characters = characterList;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Create a new movie
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Delete a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(id);
            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }
    }
}
