﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EF_Core_Database.Data;
using EF_Core_Database.Models;
using EF_Core_Database.DTOs.FranchiseDTO;
using AutoMapper;
using System.Net.Mime;
using EF_Core_Database.Services;

namespace EF_Core_Database.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService franchiseService, IMapper mapper, MovieDbContext movieDbContext)
        {
            _context = movieDbContext;
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Get a specific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetFranchiseAsync(id);


            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Reporting method to get all movies in a specific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<Movie>>> GetAllMoviesInFranchise(int id)
        {
            var movies = await _context.Movie.Where(x => x.FranchiseId == id).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            return movies;
        }

        /// <summary>
        /// Reporting method to get all characters in a specific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInFranchise(int id)
        {

            var characters = await _context.Franchise.Where(f => f.FranchiseId == id).SelectMany(f => f.Movies).SelectMany(m => m.Characters).Distinct().ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            return characters;
        }

        /// <summary>
        /// Update a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseUpdateDTO franchiseDto)
        {
            if (id != franchiseDto.FranchiseId)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Reporting method to update movie list in a specific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moviesId"></param>
        /// <returns></returns>
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, [FromBody] int[] moviesId)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise franchiseToUpdateMovies = await _context.Franchise
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == id)
                .FirstAsync();

            List<Movie> movieList = new();

            for (int i = 0; i < moviesId.Length; i++)
            {
                Movie movie = await _context.Movie.FindAsync(moviesId[i]);
                if (movie == null)
                {
                    return BadRequest("This movie does not exists");
                }
                movieList.Add(movie);
            }

            franchiseToUpdateMovies.Movies = movieList;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }


        /// <summary>
        /// Create a new franchise
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);
            
            return CreatedAtAction("GetFranchise", new { id = domainFranchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delete a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchiseAsync(id);
            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.FranchiseId == id);
        }
    }
}
