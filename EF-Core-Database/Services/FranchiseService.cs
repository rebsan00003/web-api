﻿using EF_Core_Database.Data;
using EF_Core_Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.FranchiseId == id);
        }

        public Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchise.Include(cm => cm.Movies).ToListAsync();
        }

        public Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            return await _context.Franchise.Include(m => m.Movies).FirstOrDefaultAsync(m => m.FranchiseId == id);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public Task UpdateMoviesInFranchiseAsync(int id, [FromBody] int[] moviesId)
        {
            throw new NotImplementedException();
        }
    }
}
