﻿using EF_Core_Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
        public Task<IEnumerable<Movie>> GetAllMoviesInFranchiseAsync(int id);
        public Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int id);
        public Task UpdateMoviesInFranchiseAsync(int id, [FromBody] int[] moviesId);



    }
}
