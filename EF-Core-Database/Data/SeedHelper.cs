﻿using EF_Core_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Data
{
    //Helper class to seed data to the database
    public class SeedHelper
    {
        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            IEnumerable<Franchise> seedFranchises = new List<Franchise>()
            {
                new Franchise
                {
                   FranchiseId = 1,
                   Name = "Harry Potter",
                   Description = "A film series based on J.K. Rowling's novels with the same name. The series consists of eight fantasy films."
                },
                new Franchise
                {
                   FranchiseId = 2,
                   Name = "James Bond",
                   Description = "A film series consisting of 27 productions. All of these movies are action movies."
                }
            };
            return seedFranchises;
        }
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> seedMovies = new List<Movie>()
            {
                new Movie
                {
                    MovieId = 1,
                    Title = "Harry Potter and the Philosopher's Stone",
                    Genre = "Fantasy",
                    Year = 2001,
                    Director = "David Heyman",
                    Picture = "https://www.imdb.com/title/tt0241527/mediaviewer/rm3145094400/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi3115057433/?playlistId=tt0241527&ref_=tt_pr_ov_vi",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 2,
                    Title = "Harry Potter and the Goblet of Fire",
                    Genre = "Fantasy",
                    Year = 2005,
                    Director = "David Heyman",
                    Picture = "https://www.imdb.com/title/tt0330373/mediaviewer/rm436509952/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi2611740953/?playlistId=tt0330373&ref_=tt_ov_vi",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 3,
                    Title = "Goldeneye",
                    Genre = "Action",
                    Year = 1995,
                    Director = "Martin Campbel",
                    Picture = "https://en.wikipedia.org/wiki/GoldenEye#/media/File:GoldenEye_-_UK_cinema_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=lcOqUE0u1LM",
                    FranchiseId = 2
                }
            };
            return seedMovies;
        }

        public static IEnumerable<Character> GetCharacterSeeds()
        {
            IEnumerable<Character> seedCharacters = new List<Character>()
            {
                new Character
                {
                    CharacterId = 1,
                    FullName = "Harry Potter",
                    Alias = "The chosen one",
                    Picture = "https://en.wikipedia.org/wiki/Harry_Potter_%28character%29#/media/File:Harry_Potter_character_poster.jpg"
                },
                new Character
                {
                    CharacterId = 2,
                    FullName = "Hermione Granger",
                    Alias = "Hermy",
                    Picture = "https://en.wikipedia.org/wiki/Hermione_Granger#/media/File:Hermione_Granger_poster.jpg"
                },
                new Character
                {
                    CharacterId = 3,
                    FullName = "James Bond",
                    Alias = "007",
                    Picture = "https://www.imdb.com/title/tt2382320/mediaviewer/rm1714747649/?ref_=tt_ov_i"
                }
            };
            return seedCharacters;
        }

        public static IEnumerable<object> GetCharacterMovies()
        {
            IEnumerable<object> characterMovies = new List<object>()
            {
                new
                {
                    MovieId = 1,
                    CharacterId = 1
                },
                new
                {
                    MovieId = 1,
                    CharacterId = 2
                },
                new
                {
                    MovieId = 2,
                    CharacterId = 1
                },
                new
                {
                    MovieId = 2,
                    CharacterId = 2
                },
                new
                {
                    MovieId = 3,
                    CharacterId = 3
                }
            };
            return characterMovies;
        }

    }
}
