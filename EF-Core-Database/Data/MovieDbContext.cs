﻿using EF_Core_Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        //Tables
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }

       protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Seed data
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());

            //Seed many-to-many character-movie. And accessing linking table.
            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    mc =>
                    {
                        mc.HasKey("MovieId", "CharacterId");
                        mc.HasData(SeedHelper.GetCharacterMovies());
                    } );
        }
    }
}
