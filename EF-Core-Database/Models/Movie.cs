﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Models
{
    public class Movie
    {
        //PK
        public int MovieId { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "The title is too long")]
        public string Title { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "The genre is too long")]
        public string Genre { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "The director name is too long")]
        public string Director { get; set; }
        [MaxLength(500, ErrorMessage = "The picture url is too long")]
        [Url]
        public string Picture { get; set; }
        [MaxLength(500, ErrorMessage = "The trailer url is too long")]
        [Url]
        public string Trailer { get; set; }

        //Relationship one-to-many
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        //Relationship many-to-many
        public ICollection<Character> Characters { get; set; }


    }
}
