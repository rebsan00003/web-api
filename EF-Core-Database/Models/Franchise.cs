﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Models
{
    public class Franchise
    {
        //PK
        public int FranchiseId { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "The name is too long")]
        public string Name { get; set; }

        [MaxLength(500, ErrorMessage = "The description is too long")]
        public string Description { get; set; }
        //Relationship one-to-many
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
