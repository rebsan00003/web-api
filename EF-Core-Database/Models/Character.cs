﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EF_Core_Database.Models
{
    public class Character
    {
        //PK
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "The name is too long")]
        public string FullName { get; set; }

        [MaxLength(50, ErrorMessage = "The alias is too long")]
        public string Alias { get; set; }

        [Url]
        public string Picture { get; set; }

        //Relationship many-to-many
        public virtual ICollection<Movie> Movies { get; set; }

    }
}
